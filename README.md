# Pirhana
Blood in the water.

version 0.1
Made with love.

---
Recommended usage:

pirhana -help
> for help
pirhana -crawl
> populate a table of target forms. This will take a long time.
pirhana -entry
> add contact information to a table
pirhana -signup
> post contact information to forms.